# Import packages
import logging
import time
from google.oauth2 import service_account
import pandas as pd
from datetime import datetime, timedelta, date
from google.cloud import bigquery
import sys
import os
import pandas_gbq
from google.cloud import storage
from oauth2client.service_account import ServiceAccountCredentials

# Start time of execution of script
start = time.time()


# Set up logging, write logs to file, info and above
logging.basicConfig(level=logging.INFO, filename='app.log', filemode='w',
                    format='%(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# GCP project info and service account key
logger.info("Importing Packages and creating connections...\n")
projectid = "unravel-303411"
AUTH_FILE = "client-secret.json"
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = AUTH_FILE
credentials = service_account.Credentials.from_service_account_file(AUTH_FILE)


def fetch_bq_data(bq_query):
    """ Function to accept a string as query, fetch data from table, write result to csv file and return result DF  """

    # Create BQ client
    logger.info("Creating BQ client...\n")
    client = bigquery.Client()

    # Fetch result set as Dataframe
    logger.info("Querying the view...\n")
    logger.info("Fetching result as Pandas dataframe...\n")
    bq_data_df = pandas_gbq.read_gbq(bq_query, project_id=projectid, credentials=credentials)

    # Write pandas df to csv file
    logger.info("Writing results to csv file...\n")
    bq_data_df.to_csv('top_bq_questions.csv', encoding='utf-8', index=True)

    # return the dataframe
    logger.info("Final Dataframe...\n")
    logger.info(bq_data_df)
    return bq_data_df
    logger.info("Script successfully completed...\n")


# Main query which selects top 10 most viewed questions with BQ tag
most_viewed_questions_query = """SELECT * FROM `unravel-303411.stack_overflow.top_bq_questions` LIMIT 1000"""


# Function call with our query to fetch most viewed questions from stackoverflow with tag - Bigquery
print(fetch_bq_data(most_viewed_questions_query))


# Upload csv file to GCS
def upload_to_bucket():
    """ Upload data to a bucket"""

    storage_client = storage.Client.from_service_account_json("./client-secret.json")

    bucket = storage_client.get_bucket('unravel_python_app')
    filename = "top_bq_questions.csv"
    blob = bucket.blob(filename)
    with open('top_bq_questions.csv', 'rb') as f:
        blob.upload_from_file(f)

    # returns a public url
    # Log to file
    logger.info(blob.public_url)
    return blob.public_url


# Run time of script
end = time.time()
print(upload_to_bucket())
logger.info(f"\nExecution time: {round(((end - start) / 60),2)} mins")
